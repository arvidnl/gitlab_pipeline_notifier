let rec list_iter xs f =
  let open Gitlab.Monad in
  match xs with
  | x :: xs ->
      let* () = f x in
      list_iter xs f
  | [] -> return ()

let rec list_map_s xs f =
  let open Gitlab.Monad in
  match xs with
  | x :: xs ->
      let* x' = f x in
      let* xs' = list_map_s xs f in
      return (x' :: xs')
  | [] -> return []
