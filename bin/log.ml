type log_level = Debug | Info | Error

let current_level = ref Info

let log ?(datetime = Unix.time ()) (level : log_level) fmt =
  let print =
    match (!current_level, level) with
    | Error, Error | Info, (Info | Error) | Debug, _ ->
        fun s ->
          let s =
            "[" ^ ISO8601.Permissive.string_of_datetime datetime ^ "] " ^ s
          in
          Lwt_io.print s
    | _ -> fun _ -> Lwt.return_unit
  in
  Printf.ksprintf print fmt

let info fmt = log Info fmt
let debug fmt = log Debug fmt
let error fmt = log Error fmt
