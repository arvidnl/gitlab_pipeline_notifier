open Util

type command = string * string list

let pp_command fmt (cmd, args) =
  Format.fprintf fmt "%s %s" cmd (String.concat " " args)

let pp_code fmt proc_status =
  let open Unix in
  let status, code =
    match proc_status with
    | WEXITED n -> ("WEXITED", n)
    | WSIGNALED n -> ("WSIGNALED", n)
    | WSTOPPED n -> ("WSTOPPED", n)
  in
  Format.fprintf fmt "%s %d" status code

let check (cmd : command) (status, out_lines, err_lines) =
  match status with
  | Unix.WEXITED 0 -> out_lines
  | code ->
      let prepend prefix lines = List.map (fun line -> prefix ^ line) lines in
      failwith
        (sf
           "Expected command %a to succeed, but got status code %a and output:\n\
            %s, and error output: %s" pp_command cmd pp_code code
           (String.concat "\n" (prepend "> " out_lines))
           (String.concat "\n" (prepend "> " err_lines)))

let lwt_process_output_to_list ?env ((cmd, args) : command) =
  let open Lwt in
  let open Lwt.Syntax in
  let process =
    Lwt_process.open_process_full ?env ("", Array.of_list @@ (cmd :: args))
  in
  let chan_to_lines chan =
    let res = ref ([] : string list) in
    let rec process_otl_aux () =
      let* line = Lwt_io.read_line_opt chan in
      match line with
      | Some l ->
          res := l :: !res;
          process_otl_aux ()
      | None -> return (List.rev !res)
    in
    process_otl_aux ()
  in
  let* stdout = chan_to_lines process#stdout in
  let* stderr = chan_to_lines process#stderr in
  let* status = process#close in
  return (status, stdout, stderr)

let lwt_cmd_to_list_and_check ?env (cmd : command) =
  let open Lwt in
  let open Lwt.Syntax in
  let* status, out_lines, err_lines = lwt_process_output_to_list ?env cmd in
  return (check cmd (status, out_lines, err_lines))
