let rec take : int -> 'a Gitlab.Stream.t -> 'a list Gitlab.Monad.t =
 fun n s ->
  let open Gitlab.Monad in
  if n <= 0 then return []
  else
    let* head_opt = Gitlab.Stream.next s in
    match head_opt with
    | Some (v, s) ->
        let+ tail = take (n - 1) s in
        v :: tail
    | None -> return []
