open Util

let ( let*! ) m f =
  let open Gitlab.Monad in
  let* x = embed @@ m in
  f x

(* TODO: move to GitLab API *)
module Gitlab_shim = struct
  let project_by_short_ref ?token ~project_id () =
    let open Gitlab in
    let open Lwt in
    let uri =
      Uri.of_string
        (Printf.sprintf "https://gitlab.com/api/v4/projects/%s"
           (Uri.pct_encode project_id))
    in
    let fail_handlers =
      [ API.code_handler ~expected_code:`Not_found (fun _ -> return None) ]
    in
    API.get ?token ~uri ~fail_handlers (fun body ->
        return (Some (Gitlab_j.project_short_of_string body)))
end

module Pipeline_status = struct
  let to_string = function
    | `Created -> "created"
    | `WaitingForResource -> "waiting_for_resource"
    | `Preparing -> "preparing"
    | `Pending -> "pending"
    | `Running -> "running"
    | `Success -> "success"
    | `Failed -> "failed"
    | `Canceled -> "canceled"
    | `Skipped -> "skipped"
    | `Manual -> "manual"
    | `Scheduled -> "scheduled"

  let to_emoji = function
    | `Created -> "💭"
    | `WaitingForResource -> "🚚"
    | `Preparing -> "⏲"
    | `Pending -> "🆕"
    | `Running -> "🔄"
    | `Success -> "✅"
    | `Failed -> "💥"
    | `Canceled -> "➖"
    | `Skipped -> "⏭"
    | `Manual -> "⚙"
    | `Scheduled -> "⌛"
end

module Project_ref = struct
  type project_ref = Short_id of { owner : string; name : string } | Id of int

  let parse_short_id_exn str =
    match String.split_on_char '/' str with
    | [ owner; name ] -> Short_id { owner; name }
    | _ ->
        raise
          (Invalid_argument
             (sf "Project ref %s is not on the form [NAMESPACE]/[PROJECT]" str))

  let parse_exn project_ref =
    match int_of_string_opt project_ref with
    | Some project_id -> Id project_id
    | None -> parse_short_id_exn project_ref

  let parse_json_exn = function
    | `String s -> parse_short_id_exn s
    | `Int i -> Id i
    | v ->
        raise
          (Invalid_argument
             ("A project reference must be either integer or string, got "
            ^ Yojson.Basic.to_string v))

  let to_string = function
    | Short_id { owner; name } -> owner ^ "/" ^ name
    | Id id -> string_of_int id

  let to_id ?token =
    let open Gitlab.Monad in
    function
    | Short_id { owner; name } -> (
        let* projects =
          Gitlab_shim.project_by_short_ref ?token
            ~project_id:(owner ^ "/" ^ name)
            ()
        in
        match Gitlab.Response.value projects with
        | Some project -> return project.project_short_id
        | None ->
            raise
              (Invalid_argument
                 (sf "Could not parse %s/%s to a single project\n" owner name)))
    | Id id -> return id
end

let parse_config ?default_project_refs ?default_token () =
  let config_file = "gl-cfg.json" in
  let token = ref default_token in
  let project_refs = ref default_project_refs in
  if Sys.file_exists config_file then (
    let config_json = Yojson.Basic.from_file config_file in
    if
      Option.is_none !token
      && List.mem "gitlab-token" (Yojson.Basic.Util.keys config_json)
    then
      token :=
        Some
          Yojson.Basic.Util.(
            config_json |> member "gitlab-token" |> to_string
            |> Gitlab.Token.of_string);
    if
      Option.is_none !project_refs
      && List.mem "projects" (Yojson.Basic.Util.keys config_json)
    then
      project_refs :=
        Some
          Yojson.Basic.Util.(
            config_json |> member "projects" |> to_list
            |> List.map Project_ref.parse_json_exn));
  ( (match !token with
    | None ->
        raise
          (Invalid_argument
             (sf
                "A GitLab personal access token must be provided either \
                 through '--private-token' or in %s"
                config_file))
    | Some t -> t),
    match !project_refs with
    | None | Some [] ->
        raise
          (Invalid_argument
             (sf
                "A project refs must be provided either through '--projects' \
                 or in %s"
                config_file))
    | Some ids -> ids )

let notify ~summary ~body =
  let open Lwt.Syntax in
  let cmd = ("notify-send", [ summary; body ]) in
  let v = "DBUS_SESSION_BUS_ADDRESS" in
  let env = [| v ^ "=" ^ Option.value ~default:"" @@ Sys.getenv_opt v |] in
  let* _ = Proc.lwt_cmd_to_list_and_check ~env cmd in
  Lwt.return_unit

let () =
  let project_refs = ref None in
  let token = ref None in
  let username = ref None in
  let polling_interval = ref 60.0 in
  let no_emojis = ref false in
  let args_spec =
    [
      ( "-i",
        Arg.Unit (fun () -> Log.current_level := Info),
        "Set log level info (default)." );
      ( "-v",
        Arg.Unit (fun () -> Log.current_level := Debug),
        "Set log level debug." );
      ( "-s",
        Arg.Unit (fun () -> Log.current_level := Error),
        "Set log level to error." );
      ( "--username",
        Arg.String (fun s -> username := Some s),
        "<TOKEN> GitLab username." );
      ( "--interval",
        Arg.Float (fun f -> polling_interval := f),
        sf "<SECONDS> Polling interval. Default, %f." !polling_interval );
      ( "-t",
        Arg.Float (fun f -> polling_interval := f),
        "<SECONDS> same as --interval." );
      ( "--private-token",
        Arg.String (fun t -> token := Some (Gitlab.Token.of_string t)),
        "<TOKEN> GitLab private token." );
      ( "--projects",
        Arg.String
          (fun ids ->
            project_refs :=
              Some
                (List.map Project_ref.parse_exn @@ String.split_on_char ',' ids)),
        "<PROJECT_ID> GitLab project reference: either a project id or a \
         namespace/name short ref." );
      ("--no-emojis", Arg.Set no_emojis, "Disable emojis.");
    ]
  in
  let usage_msg =
    Printf.sprintf
      "Usage: %s [options]\n\n\
       gitlab-pipeline-notifier watches the pipelines in the specified \
       projects periodically (by default, every minute), optionally belonging \
       to the specified user, for status updates. It sends a notification \
       using 'notify-send' when:\n\n\
      \ - pipelines that start running\n\
      \ - pipelines that terminate with success or failure.\n\n\
       In addition to options below, gitlab-pipeline-notifier can be \
       configured in the 'gl-cfg.json' in current working directory. Here's an \
       annotated example;\n\n\
       {\n\
      \    // A GitLab personal access token, which must be provided either on \
       the\n\
      \    // command line or here\n\
      \    \"gitlab-token\": \"glpat-...\",\n\
      \    // A list of project references. Projects can be referred to either \
       by their\n\
      \    // short id ('[NAMESPACE]/[PROJECT_NAME]') or by their numeric \
       project id.\n\
      \    \"projects\": [\n\
      \        9487506,\n\
      \        \"tezos/tezos\",\n\
      \        \"tezos/opam-repository\",\n\
      \        \"tezos/teci\",\n\
      \        \"tezos/tezos-sw-metrics\"\n\
      \    ]\n\
       }\n\n\
       Both keys 'gitlab-token' and 'projects' are optional. Command-line \
       arguments take precedence over the configuration file.\n\n\
       Options are:" Sys.argv.(0)
  in
  Arg.parse args_spec
    (fun s -> raise @@ Invalid_argument (sf "No anonymous arguments: %s" s))
    usage_msg;
  let token, project_refs =
    parse_config ?default_project_refs:!project_refs ?default_token:!token ()
  in
  let username = !username in
  let polling_interval = !polling_interval in

  let tbl = Hashtbl.create 5 in
  let rec poll project_ids_opt =
    let open Gitlab.Monad in
    catch
      (fun () ->
        match project_ids_opt with
        | None ->
            let* project_ids =
              Gitlab_monad_util.list_map_s project_refs (fun id_s ->
                  let* id = Project_ref.to_id ~token id_s in
                  return (id, id_s))
            in
            let*! () =
              Log.info "Watching for changes in projects %s...\n"
                (String.concat ", "
                   (List.map Project_ref.to_string project_refs))
            in
            poll (Some project_ids)
        | Some project_ids ->
            let* pipelines =
              let pipelines project_id =
                Gitlab.Project.pipelines ?username ~token ~project_id
                  ~order_by:`Updated_at ~sort:`Desc ~per_page:10 ()
              in
              let rec aux acc = function
                | [] -> return acc
                | (project_id, _) :: project_ids ->
                    let* ps =
                      pipelines project_id |> Gitlab_stream_util.take 10
                    in
                    aux (ps @ acc) project_ids
              in
              aux [] project_ids
            in
            let to_emoji_opt status =
              if !no_emojis then "" else Pipeline_status.to_emoji status ^ " "
            in
            let* () =
              Gitlab_monad_util.list_iter pipelines
              @@ fun (pipeline : Gitlab_t.pipeline) ->
              let id = pipeline.id in
              let status = pipeline.status in
              let log_status_change level ?old_status
                  (pipeline : Gitlab_t.pipeline) =
                Log.log ~datetime:pipeline.updated_at level
                  "#%d: %s%s -> %s %s(%s)\n" pipeline.id
                  (Option.fold ~none:"" ~some:to_emoji_opt old_status)
                  (Option.fold ~none:"_" ~some:Pipeline_status.to_string
                     old_status)
                  (Pipeline_status.to_string pipeline.status)
                  (to_emoji_opt pipeline.status)
                  pipeline.web_url
              in

              let old_status = Hashtbl.find_opt tbl id in
              let*! () = log_status_change Debug ?old_status pipeline in
              let notify_status ?extra_info (pipeline : Gitlab_t.pipeline)
                  status_verb =
                let extra_info =
                  Option.fold ~none:"" ~some:(fun s -> ", " ^ s) extra_info
                in
                let project_name =
                  List.assoc_opt pipeline.project_id project_ids
                  |> Option.value ~default:(Project_ref.Id pipeline.project_id)
                  |> Project_ref.to_string
                in
                let*! () =
                  notify
                    ~summary:
                      (sf "%sPipeline #%d in project %s %s%s"
                         (to_emoji_opt pipeline.status)
                         pipeline.id project_name status_verb extra_info)
                    ~body:(sf "See %s for more information." pipeline.web_url)
                in
                let*! () =
                  Log.info "#%d: %sPipeline %s (%s)%s\n" pipeline.id
                    (to_emoji_opt pipeline.status)
                    status_verb pipeline.web_url extra_info
                in
                return ()
              in
              let* () =
                match (old_status, status) with
                | None, `Running -> notify_status pipeline "started"
                | None, _ -> return ()
                | Some status', _ when status <> status' -> (
                    match status with
                    | `Success -> notify_status pipeline "succeeded"
                    | `Failed ->
                        let* failed_jobs =
                          return
                          @@ Gitlab.Project.pipeline_jobs ~token
                               ~project_id:pipeline.project_id
                               ~pipeline_id:pipeline.id ~scope:`Failed
                               ~include_retried:true ()
                        in
                        let* failed_jobs = Gitlab.Stream.to_list failed_jobs in
                        let extra_info =
                          "failing jobs: " ^ String.concat ", "
                          @@ List.map
                               (fun ({ name; _ } : Gitlab_t.pipeline_job) ->
                                 name)
                               failed_jobs
                        in
                        notify_status pipeline ~extra_info "failed"
                    | `Running -> notify_status pipeline "started"
                    | _ -> return ())
                | Some _, _ -> return ()
              in
              Hashtbl.replace tbl id status;
              return ()
            in
            let*! () =
              Log.debug "Sleeping for %.2f seconds...\n" polling_interval
            in
            let*! () = Lwt_unix.sleep polling_interval in
            poll (Some project_ids))
      (fun e ->
        let*! () = Log.error "Exception: %s\n" (Printexc.to_string e) in
        let*! () =
          Log.debug "Trying again in %.2f seconds...\n" polling_interval
        in
        let*! () = Lwt_unix.sleep polling_interval in
        poll project_ids_opt)
  in
  Lwt_main.run @@ Gitlab.Monad.run (poll None)
