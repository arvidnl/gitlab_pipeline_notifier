#!/bin/sh

set -eu

cd /home/arvid/dev/gitlab_pipeline_notifier/bin || exit 1

tmux new-session -d -s gitlab-pipeline-notifier 'opam exec -- dune exec ./main.exe -- --username arvidnl -t 20; exec bash'
