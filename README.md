`gitlab-pipeline-notifier` watches the pipelines in the specified projects periodically (by default, every minute), optionally belonging to the specified user, for status updates. It sends a notification using 'notify-send' when:

 - pipelines that start running
 - pipelines that terminate with success or failure.

![A screenshot of GitLab Pipeline Notifier](screenshots/gitlab_pipeline_notifier.png)

# Installation

## From sources using `opam`

```
opam switch create . 4.14.0
opam install dune . --deps-only
dune build
```

# Usage

Usage: `dune exec bin/main.exe -- [options]`

# Starting in the background on startup

See the script `scripts/gitlab-pipeline-notifier.sh`. This starts the
notifier in a detached tmux session. Run this script on startup (using
e.g. `startup` for Ubuntu). You can now attach to the notifier session
by issuing:

    tmux attach-session -t gitlab-pipeline-notifier

# Configuration file

In addition to options below, `gitlab-pipeline-notifier` can be configured in the `gl-cfg.json` in current working directory. Here's an annotated example;

```javascript
{
    // A GitLab personal access token, which must be provided either on the
    // command line or here
    "gitlab-token": "glpat-...",
    // A list of project references. Projects can be referred to either by their
    // short id ('[NAMESPACE]/[PROJECT_NAME]') or by their numeric project id.
    "projects": [
        9487506,
        "tezos/tezos",
        "tezos/opam-repository",
        "tezos/teci",
        "tezos/tezos-sw-metrics"
    ]
}
```

Both keys `gitlab-token` and `projects` are optional. Command-line arguments take precedence over the configuration file.

# Options

Options are:

  - `-i` Set log level info (default).
  - `-v` Set log level debug.
  - `-s` Set log level to error.
  - `--username <TOKEN>` GitLab username.
  - `--interval <SECONDS>` Polling interval. Default, 60.000000.
  - `-t <SECONDS>` same as `--interval`.
  - `--private-token <TOKEN>` GitLab private token.
  - `--projects <PROJECT_ID>` GitLab project reference: either a project id or a namespace/name short ref.
  - `-help`  Display this list of options
  - `--help`  Display this list of options
